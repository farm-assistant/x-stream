"use strict";


var ARQ = require('./lib/xarq').ARQ;
var xStreamManager = require('./lib/xstream-manager').xStreamManager;
var xBeeStream = require('./lib/xstream').xBeeStream;
var SerialPort = require('serialport').SerialPort;
var xbee_api = require('xbee-api');
var C = xbee_api.constants;

/**
 * Open the serial connection to the XBee modem
 * On request from an XBee device, establish a
 * stream using Unix sockets.
 * 
 **/

var warning =  '\n\n\nxbee-unix-stream  Copyright (C) 2015  Andrew McCure <andrew@agsense.co.nz>\n\n' + 
                'This software comes with ABSOLUTELY NO WARRANTY.\n' + 
                'This is free software, and you are welcome ' +
                'to redistribute it under certain conditions.\n\n' + 
                'For commercial use, please contact the author for a license.\n\n\n';
                

var run = function (options) {

    var $this = this;
    
    options.baud  = options.baud   || 115200;
    options.delay = options.delay || 500;
    options.socketPath = options.socketPath || "/tmp/sockets";
    if (!options.hasOwnProperty("port")){
        throw new Exception("No serial port specified");
    }

    var xbeeAPI = new xbee_api.XBeeAPI({
      api_mode: 2,
      raw_frames:false
    });
    console.log(options.baud);
    var serialport = new SerialPort(options.port, {
      baudrate: options.baud,
      buffersize:512,
      parser: xbeeAPI.rawParser()
    });
    
    var xstream = new xBeeStream(xbeeAPI,serialport)
    this.streams = {};
    
    this._cleanup = function () {
        process.removeListener('SIGINT',$this._cleanup)
        process.removeListener('SIGTERM',$this._cleanup)
        for (var key in $this.streams){
            $this.streams[key].cleanUpAsync();
        }
        process.exit(0);
    }
    
    
    try{
        xbeeAPI.on("frame_object", function(_frame) {
            if (_frame.type == C.FRAME_TYPE.ZIGBEE_RECEIVE_PACKET){
                // check dest address
                var address64  = _frame.remote64;
  
                if (!$this.streams.hasOwnProperty(address64)){
                    // Create UNIX pipe
                     $this.streams[address64] = true;
                     $this.streams[address64] = new xStreamManager(xstream,
                                                            new ARQ(options),
                                                            address64,
                                                            options);
                     
                     $this.streams[address64].on('end',function(){ // Todo: Add timeout and emit end from xStreamManager
                            delete $this.streams[address64];
                    });
                }
                xstream.emit(address64,_frame.data);
            }
        });
        xbeeAPI.on("error", function(e) {
            if (!e.toString().indexOf("Checksum") > 0)
                throw(e);
        });
    } catch(e){
        console.error(e.stack);
    } finally{
        console.log(warning);
        process.on('SIGINT',$this._cleanup);
        process.on('SIGTERM',$this._cleanup);
    }
}    


module.exports.run = run;
