"use strict";

var Events = require('events').EventEmitter;
var net = require('net');
var fs = require('fs');

function createSocketsFolder(path, mask, cb) {
    if (typeof mask == 'function') { // allow the `mask` parameter to be optional
        cb = mask;
        mask = '0777';
    }
    fs.mkdir(path, mask, function(err) {
        if (err) {
            if (err.code == 'EEXIST') cb(null); // ignore the error if the folder already exists
            else cb(err); // something else went wrong
        } else cb(null); // successfully created folder
    });
}



function xStreamManager(xstream,xarq,address64,options){
    
    Events.call(this);
    this.xstream = xstream;
    this.path = options.socketPath + "/" + address64.toString();
    this.path = this.path.toString();
    this.address64 = address64;
    this.arq = xarq;
    var $this = this;

    createSocketsFolder(options.socketPath,'0744',function(err){
        if (err){
         console.error(err);
         process.exit(1);
        }
    });
    

    this.xstream.on(this.address64,function(data){
        $this.arq.emit('readPhysical',data);    
    });
    
    this.arq.on('writePhysical',function(data){
            console.log("writePhysical " + $this.address64);
            $this.xstream.write(data,$this.address64);
    });

    this.arq.on('end',function(){
        console.log("Tearing down stream for " + $this.address64);
         delete $this.server;
         $this.emit('end');
    });

//    console.log("Trying to create listener for " + $this.path);
    this.server = net.createServer(function(socket) {
    
        $this.socket = socket;
        $this.buffer  = [];
        
        console.log("Connection established: " + $this.path);
        
        var _write = function(data){
            if  ($this.socket.writable) {
             //   console.log("Writing to UNIX socket");
                $this.socket.write(data,data.length);
            } else {
               // console.log("Unable to write to socket");
            }
        }
        
        var _paused = function () {
            setTimeout(function(){
                if ($this.arq.readySync()){
                    if ($this.buffer.length >= 0)
                        $this.arq.emit('data',[]); // flush buffer
                    $this.socket.resume();
                    console.log("Resuming " + $this.path);
                } else {
                    // Add timer/abort?
                    _paused(); // Call again
                }
            },500);
        }
          
        var _read = function (data){
                  //$this.arq.emit('readNetwork',data);
               for(var i=0; i<data.length; i++) 
                     $this.buffer.push(data[i]); 

               if ($this.buffer.length == 0)
                    return;
                
               if ($this.arq.readySync()){
                    console.log("Writing data " + $this.buffer);
                    var written =  $this.arq.writeSync(new Buffer($this.buffer));
                    $this.buffer = $this.buffer.slice(written);
               } else {
                    console.log("Pausing");
                    $this.socket.pause();            
               }
        };

        $this.arq.on('writeNetwork',_write);;
        socket.on('data',_read);;

        socket.on('end',function(){
          console.log("Connection disconnected: " + $this.path); 
          $this.arq.removeListener('writeNetwork',_write);
          //$this.arq.removeListener('data',_read);
        })

     });
    
    this.deletePathAsync = function (){
        if (fs.existsSync($this.path)) {
            console.log("Deleting existing path");
            fs.unlinkSync($this.path);
        }
     };
     
    this.deletePathAsync();

    this.server.listen($this.path,function() {
        console.log('Server listening on ' + $this.path);
    });

    this.server.on('end',function(){
        $this.deletePathAsync();
    });
    
    this.cleanUpAsync = function (){
            $this.server.close();
            //$this.deletePathAsync();
            
    }
}


xStreamManager.prototype = Object.create(Events.prototype);
module.exports.xStreamManager = xStreamManager;
    
