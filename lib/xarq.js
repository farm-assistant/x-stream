"use strict";

/**
 * XARQ
 *
 * Implements a Selective Repeat Serial protocols
 * Protocol is symmetrical; binary; and reliable
 *
 **/

var util = require('util');
var Events = require('events').EventEmitter;
var crc  =require('crc');

var DEBUG = false;



var log = function (s){
    if (DEBUG)
        console.log(s);
}

var TEST = false;
var MAX_TXNQ_LENGTH = 500

var DG_HEADER   =   6;
var DG_PAYLOAD  =   96;
var DG_SIZE = DG_HEADER + DG_PAYLOAD;

// Header constants
var DG_CRC           = 0; // Checksum
var DG_WID           = 1; // WinId (use high and low 4 bits separately)
var DG_MASK          = 2; // Ack Mask
var DG_CNTRL_PARAM   = 2;
var DG_CNTRL_PARAM_2 = 3;
var DG_ACKED_MASK    = 3; // Acked Mask
var DG_LEN           = 4; // Data Length
var DG_RAND          = 5; // Data Length


// Control Frame Types
var F_CNTRL         = 0xF;
var F_PING          = 0x1;
var F_PONG          = 0x2;
var F_SLEEP         = 0x3;


// ARQ Constants
var ARQ_MAX     = 14;
var ARQ_WMAX    = ARQ_MAX / 2;
var ARQ_UNPUNG  = 0;
var ARQ_SENDNEW = 1;
var ARQ_REQACK  = 2;
var ARQ_RESEND  = 3;
var ARQ_ACKED   = 4;
var ARQ_IDLE    = 400; // ms of idle time before queuing remaining input data


// DATA FRAME [CRC,AckWinId <<4+ WinId, Mask,  AckMask, Len, Data ...  ]
// CNTL FRAME [CRC,CNTLID <<4 +FUNC, Param1, Param2, Len,  Data ... ]

function DataGram(){
    
    Events.call(this);
    var $this = this;
    this._sent = false;
    this._data = null        
    this._frameId = 0;
    this._ack = false;
    this._len =0;
    
    // returns first set bit from right
    // or -1 if no bits set
    this._getLastSetBit = function (mask) {
        // Assert mask !=0?
        if (mask == 0)
            return -1;
        var offset = 8-ARQ_WMAX;
        while( (mask & 0x1 << offset) == 0  && offset <= ARQ_WMAX)
            offset ++;
        return ARQ_WMAX - offset;
    };

    // returns first set bit from left
    // or -1 if no bits set
    this._getFirstSetBit = function (mask) {
        if (mask == 0)
            return -1;
        var offset = 0;
        while(mask >> (8 - offset) & 0x1 == 0  && offset <8)  
            offset ++;
        return offset;
    };

    this._setWidLo = function (val) {
        $this._data[DG_WID] = ($this._data[DG_WID] & 0xF0) + val;
    };

    this._getWidLo = function () {
        return $this._data[DG_WID] & 0x0F;
    };

    this._setWidHi = function (val) {
        $this._data[DG_WID] = ($this._data[DG_WID] & 0x0F) + (val << 4);
    };

    this._getWidHi = function () {
        return $this._data[DG_WID] >> 4;
    };
    
    this.setControlParam = function (val) {
        $this._data[DG_CNTRL_PARAM] =  val;
    };

    this.setControlParam2 = function (val) {
        $this._data[DG_CNTRL_PARAM_2] =  val;
    };

    this.getControlParam = function () {
        return $this._data[DG_CNTRL_PARAM];
    };

    this.getControlParam2 = function () {
        return $this._data[DG_CNTRL_PARAM_2];
    };

    this.isSent = function () {
        return $this._sent;
    };
    
    this.setSent = function (state) {
        $this._sent = state;
    };
    
    this.setWinId = function (winId) {
        $this._data[DG_WID] = winId;
    };
    
    this.getWinId = function (offset) {
         return  $this._getWidLo();
    };

    this.getWinOffset = function () {
        return $this._getLastSetBit($this._data[DG_MASK] ^ 0x1); // convert to 0 offset
    };

    this.getFrameId = function () {
        return $this.getWinId() + $this.getWinOffset();
    };

    this.setAckedWinId = function (winId) {
        $this._setWidHi(winId);
    };
    
    this.getAckedWinId = function () {
        return $this._getWidHi();
    };

    this.getAckedWinIdSize = function () {
        return $this.getAckedWinId() + $this.getAckedWinOffset();
    };

    this.ackRequested = function () {
        return $this._data[DG_MASK] & 0x1;
    };

    this.ackReceived = function () {
        return $this._data[DG_ACKED_MASK] > 0;
    };

    this.getMask = function () {
        return $this._data[DG_MASK] ^ 0x1;
    };

    this.getAckedMask = function () {
        return $this._data[DG_ACKED_MASK] ^ 0x1;
    };
    
    this.payload = function () {
        return $this._data.slice(DG_HEADER,DG_HEADER+$this.getDataLength());
    };

    this.getDataLength = function () {
        return  $this._data[DG_LEN];
    };

    this.setDataLength = function (len){    
            $this._data[DG_LEN] = len; // Length of frame
    };
    
    this._rand = function (){    
            $this._data[DG_RAND] = parseInt(Math.random() *255);
    };

    this._pad = function (length){
        for(var i=DG_HEADER + $this._data[DG_LEN];
            i <  length;
            i++) {
                $this._data[i] = 0x0; // Math.round(Math.random() * 0xFF);
        }
    };

    this.calcCRC8 = function(length){
        $this._pad(length);
        $this._rand();
        $this._data[DG_CRC] = crc.crc8($this._data.slice(1,length));
    };

    this.valid = function(length){
        return $this._data[DG_CRC] == crc.crc8($this._data.slice(1,length));
    };

    this.frame = function(){
      return $this._data;  
    };

    this.isControlFrame = function () {
        return this._getWidHi() == F_CNTRL;
    };
}


function RxGram(data){
    Events.call(this);
    DataGram.call(this);
    var $this = this;
    this._data = new Buffer(data);
    //log("Buffer: " + this._data);

    this.isAckReceived = function(offset){
         return $this._data[DG_ACKED_MASK] >> (ARQ_WMAX-offset) & 0x1;
    }

    this.hasData = function(){
        return $this.getWinOffset() >=0 && $this.getWinOffset() <= ARQ_WMAX && $this.getDataLength() >0;
    }
}


function TxGram(data,end){
        DataGram.call(this);
        var $this = this;

        this._len = data.length+DG_HEADER; // + header length
        this._data = new Buffer(DG_HEADER + DG_PAYLOAD);
        this._end = end;
        this._attempts = 0;
        this._acked = false;
        this._data[DG_MASK] = 0x0;
        this._data[DG_ACKED_MASK] = 0x0;
        
        data.copy(this._data,DG_HEADER);

        this.setWinId(0);   
        this.setAckedWinId(0);
        this.setDataLength(data.length);
        
        this.isEnd = function (){
            return $this._end;
        }

        this.incrAttempts = function(){
            $this._attempts ++;
        }
        
        this.attempts = function (){
            return $this._attempts;
        }
            
        this.setMask = function (offset,ack){
            // assert offset < ARQ_WMAX
            if (offset >=0 && offset < ARQ_WMAX)
                $this._data[DG_MASK]  = $this._data[DG_MASK] | 0x1 << (ARQ_WMAX-offset);
            $this._data[DG_MASK] = (ack) ?
                $this._data[DG_MASK] | 0x1 :
                $this._data[DG_MASK] & 0xFE;
        };

        this.setAckedMask = function (offset,ack){
            // assert offset < ARQ_WMAX
            if (offset >=0 && offset < ARQ_WMAX)
                $this._data[DG_ACKED_MASK]  = this._data[DG_ACKED_MASK] | (0x1 << (ARQ_WMAX-offset));
            if (ack)
               $this._data[DG_ACKED_MASK] = this._data[DG_ACKED_MASK] | 0x1;
        };
        
        this.isAcked = function(offset){
             return  $this._data[DG_MASK] & (0x1 << (ARQ_WMAX-offset));
        };

        this.mergeAcked = function (txGram){
            $this.setAckedMask(txGram.getAckedMask());  
            $this.setAckedWinId(txGram.getAckedWinId());
        };

        this.unmergeAcked = function (){
            $this._data[DG_ACKED_MASK] = 0x0;
            $this.setAckedWinId(0);
        };

};
 

DataGram.prototype = Object.create(Events.prototype);
TxGram.prototype   = Object.create(DataGram.prototype);
RxGram.prototype   = Object.create(DataGram.prototype);

if (typeof Array.prototype.peek!= 'function') {
    Array.prototype.peek = function(){
     return this.length > 0 ? this[0] : null;
    }
}

if (typeof String.prototype.isEqual!= 'function') {
    String.prototype.isEqual = function (str){
        return this.toUpperCase()==str.toUpperCase();
     };
}

ARQ.prototype = Object.create(Events.prototype);

function ARQ(options){
    Events.call(this);
    var txq = [];

    var $this       = this;
    this.options    = options;
    this.tickInterval = 115200/options.baud*options.delay;
    DEBUG = options.debug | DEBUG;
    
    var timeout;

    this.loop       = 0;

    this.txOffset   = 0;
    this.txStart    = 0; // First frame Id of tx Window being sent
    this.txState    = ARQ_UNPUNG;
    this.txAcked    = false;
    this.txSize     = 0;
    this.txFrames   = []; 
    this.rxFrames   = {};
    this.akFrames  = []; 

    this.txAckRequests   = 0;  // Keep track of dups 
    
    this.rxStart    = 0; // First winId of expected rx Window 
    this.rxAcked    = false; //
    this.rxSize     = 0;

    process.on('SIGHUP',function(){
       console.log("Send sleep");
       $this.sendControl(F_SLEEP,1,1);
    });

    
    var time = function(){
        return Date.now();
    };
    

    var sendDown =  function (dgram,length){
        dgram.calcCRC8(length);

        log(dgram.frame().slice(0,length));
        $this.emit('writePhysical',dgram.frame().slice(0,length));
    }

    var sendUp = function(data){
        log("Sending Up Payload: " + data.length);
        $this.emit('writeNetwork',data);
    }
   
    this.on('readPhysical',function(data){
            var rxGram = new RxGram(data,data.length);
           // if  ( Math.random() < 0.15 )
             //   return;
            $this.emit('physical',rxGram);
    });

    //
    
    this.readySync = function () {
       return  txq.length < MAX_TXNQ_LENGTH;
    }
    
    this.writeSync = function (data) {
        timeout = time();
        var written = 0;
        for(var i=0; i< data.length && $this.readySync(); i++){
          //log("Pushing " + data[i]);
          txq.push(data[i]);
          written++;
        }
        return written;
    }

    this.on('readNetwork',$this.writeSync);
/*    
    this.on('readNetwork',function(data){
        timeout = time();
        for(var i=0;i< data.length; i++){
          log("Pushing " + data[i]);
          txq.push(data[i]);
        }
    });
*/
    this.on('network',function(data,end){
        $this.txFrames.push(new TxGram(data,end));
    });

        
/*    this.network.on('close',function(){
          log("CLOSE");
    });

    this.network.on('error',function(){
          log("ERROR");
    });
*/
    this.reset = function (state) {    
        timeout = time();
        $this.txOffset   = 0;
        $this.txStart    = 0; // First frame Id of tx Window being sent
        $this.txState    = state;
        $this.txAcked    = false;
        $this.txSize     = 0;
        $this.txFrames   = []; 
        $this.rxFrames   = {};
        $this.akFrames  = []; 
        
        $this.rxStart    = 0; // First winId of expected rx Window 
        $this.rxAcked    = false; //
        $this.rxSize     = 0;
        $this.lastState  = -1;
    };
    
    this.sendControl = function (ctype,param,param2) {
        var txGram = new TxGram(Buffer([]),true);
        txGram._setWidHi(F_CNTRL);
        txGram._setWidLo(ctype);
        if (param != undefined)
            txGram.setControlParam(param == undefined ? 0x0:param)
        if (param2 != undefined)
            txGram.setControlParam2(param2 == undefined ? 0x0:param2)
        sendDown(txGram,DG_HEADER);
        };

    
    this.ping = function (param) {
        log("PING!");
        if (param)
            $this.reset(ARQ_UNPUNG);
        $this.sendControl(F_PING,param);
    };

    this.pong = function (param) {
        log("PUNG! " + param);
//        if ($this.txState == ARQ_UNPUNG)
//            $this.reset(ARQ_SENDNEW);
        $this.sendControl(F_PONG,param);
    };
    
    this.showState = function () {
        if ($this.txState == $this.lastState)
            return;
        $this.lastState = $this.txState;
        switch($this.txState ){
            case ARQ_UNPUNG:
                log("State: ARQ_UNPUNG");
                break;
            case ARQ_SENDNEW:
                log("State: ARQ_SENDNEW");
                break;
            case ARQ_REQACK:
                log("State: ARQ_REQACK");
                break;
            case ARQ_RESEND:
                log("State: ARQ_RESEND");
                break;
            case ARQ_ACKED:
                log("State: ARQ_ACKED");
          } 
    };
    



    this.on('loop',function(){

        $this.showState();
        switch($this.txState){
            case ARQ_UNPUNG:
                $this.ping(0x1);
                break;
 
 
            case ARQ_SENDNEW:
                if ($this.txFrames.hasOwnProperty($this.txOffset)
                    && $this.txFrames[$this.txOffset].isSent() == false) {
                    
                    var ack = $this.txFrames[$this.txOffset].isEnd() || $this.txOffset == ARQ_WMAX -1;  
                    $this.txFrames[$this.txOffset].setMask($this.txOffset,ack);
                    log("Mask " + $this.txFrames[$this.txOffset].getMask());
                    $this.txFrames[$this.txOffset].setWinId($this.txStart);
                    $this.txFrames[$this.txOffset].incrAttempts();
                    
                    if ( $this.akFrames.length ){
                            $this.txFrames[$this.txOffset].mergeAcked($this.akFrames.shift());
                    }

                    sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                    $this.txSize =  $this.txOffset + 1;

                    if (ack){
                        $this.txState = ARQ_REQACK;
                        $this.txAckRequests   = 0;  // Keep track of dups 

                    } else {
                        $this.txOffset = $this.incrOffset($this.txOffset);
                    }
                } else if ( $this.akFrames.length ){
                    log("Sending ACK");
                    sendDown($this.akFrames.shift(),DG_HEADER);
                }

                break;
            
            case ARQ_REQACK:
                // Repeat Ack request
                if ($this.txFrames.hasOwnProperty($this.txOffset) && $this.txFrames[$this.txOffset].ackRequested()){
                    var len = $this.txFrames[$this.txOffset].getDataLength();
                    if ( $this.akFrames.length ){
                            $this.txFrames[$this.txOffset].mergeAcked($this.akFrames.shift());
                            sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                            $this.txFrames[$this.txOffset].unmergeAcked();
                            $this.txFrames[$this.txOffset].incrAttempts();
                    } else {
                        if ( $this.txFrames[$this.txOffset].attempts() >= 3 ){    
                            console.error("Ack requesting: " + $this.txFrames[$this.txOffset].attempts());
                            sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                        }
                        $this.txFrames[$this.txOffset].incrAttempts();
                    }
                $this.txFrames[$this.txOffset].setDataLength(len);
                $this.txAckRequests++;
                } else if ( $this.akFrames.length ){
                    sendDown($this.akFrames.shift(),DG_HEADER);
                } else {
                    console.error("Frame not found " + $this.txOffset);
                }
                break;
            
            case ARQ_RESEND:
                var sent=false;
                while(!sent){
                    if ($this.txFrames.hasOwnProperty($this.txOffset)
                        && $this.txFrames[$this.txOffset]._acked == false){

                            sent=true;
                            if ( $this.akFrames.length >0 ){
                                log("Merging");
                                $this.txFrames[$this.txOffset].mergeAcked($this.akFrames.shift());
                                sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                                $this.txFrames[$this.txOffset].unmergeAcked();
                                $this.txFrames[$this.txOffset].incrAttempts();
                            } else {
                                sendDown($this.txFrames[$this.txOffset],DG_SIZE);
                                $this.txFrames[$this.txOffset].incrAttempts();
                            }

                        if ($this.txFrames[$this.txOffset].ackRequested())
                            $this.txState = ARQ_REQACK;
                    }
                    if ($this.txState == ARQ_RESEND)
                        $this.txOffset = $this.incrOffset($this.txOffset);
                }
                
                if ( !sent && $this.akFrames.length ){
                    sendDown($this.akFrames.shift(),DG_HEADER);
                }
                break;

            case ARQ_ACKED:
                log("Window Sent!");
                if ( $this.akFrames.length ) { 
                    sendDown($this.akFrames.shift(),DG_HEADER);
                }
                $this.txFrames.splice(0,$this.txSize); // purge head of queue
                $this.txState = ARQ_SENDNEW;
                $this.txStart = $this.incr($this.txStart,$this.txSize);   
                $this.txSize  = 0;
                $this.txOffset = 0;
                $this.txAcked = false;
                break;
        }
        
        // Check max attempts?
        
       // $this.showState();
    });
    


    this.on('physical',function(rxGram){
        // Check checksum!
        
        var txGram;
        
        
        
        if (!(rxGram.valid(DG_HEADER) || rxGram.valid(DG_SIZE))){
            log("Invalid frame - ignoring");
            return;
        }
        
        log("Is ControlFrame :" + rxGram.isControlFrame());
        if ( rxGram.isControlFrame() && rxGram._getWidLo() == F_PING ) {
            if (rxGram.getControlParam() == 0x1 || $this.txState == ARQ_UNPUNG){
                console.log("Resetting");
                $this.reset(ARQ_SENDNEW);
                $this.pong(0);   
            } else
                $this.pong(0);
            return;
        }

        if ( rxGram.isControlFrame() && rxGram._getWidLo() == F_PONG ) {
            log("PONGED!");
            $this.reset(ARQ_SENDNEW);
            return;
        }

        if ( $this.txState == ARQ_UNPUNG ) {
            $this.ping(0x1);// remove?
            return;
        }
        
        if ( rxGram.ackReceived() ){
            // Handle Acknowledgements
            log("txSize :" + $this.txSize);
            if ($this.txState == ARQ_REQACK){
                var acked = true;
                if (txGram.getWinId() !=  rxGram.getAckedWinId()){
                    console.error("Received ack for wrong window");
                    acked = false;
                } else {
                    for(var i=$this.txSize-1; i>=0 ; i--){
                      $this.txFrames[i]._acked = rxGram.isAckReceived(i);
                      log(i + " : " + $this.txFrames[i]._acked);
                      if (acked && $this.txFrames[i]._acked != acked){
                        // request ack for last unacked frame frame
                        // and recalculate CRC
                        $this.txFrames[i].setMask(i,true);
                        acked = false;
                      }
                    }
                }
                log("Window " + $this.txStart + " - Acked " + acked);
                $this.txState = (acked) ? ARQ_ACKED:ARQ_RESEND;
            }
        }
        

        if ( rxGram.hasData() ){
           // Handle data
           
            var offset   = rxGram.getWinOffset();
            console.log("Received: " + rxGram.getWinId() + " offset: " +  rxGram.getWinOffset() );
            log(util.inspect(rxGram.frame()));
          if ( $this.rxStart == rxGram.getWinId() ) { // Receviving current frame
                if (!$this.rxFrames.hasOwnProperty(offset)){
                    log("Add frame at offset: " + offset);
                    $this.rxFrames[offset] = rxGram;
                } else {
                    log("Frame already received: " );
                }
            } else if ( $this.rxAcked // New rx window started
                && $this.inRange(rxGram.getWinId(), $this.incr($this.rxStart,$this.rxSize),
                                        $this.incr($this.incr($this.rxStart,$this.rxSize),ARQ_WMAX)                 
                                )) {
                // Handle new frame
                    log("Create new frame set");
                    for(var i=0; i< ARQ_WMAX;i++){
                        if ($this.rxFrames.hasOwnProperty(i))
                            delete $this.rxFrames[i];
                    }
                    // Create first frames
                    $this.rxFrames[offset] = rxGram;
                    $this.rxStart          = rxGram.getWinId();
                    $this.rxSize           = 0;
                    $this.rxAcked          = false;
            } else {
                console.error("Receiving unexpected frame - window Id: " + rxGram.getWinId() + " expected " + $this.rxStart);
            }
        }

            // Handle Ack requests
            // 2358745

        if ( rxGram.ackRequested()  ) {
            log("rxAcked " + $this.rxAcked )
            log("rxStart " + $this.rxStart )
            log("rxSize " + $this.rxSize  )

            offset   = rxGram.getWinOffset();
            $this.rxSize = ($this.rxSize == 0) ? (offset+1) : $this.rxSize;
            txGram = new TxGram(new Buffer([]),false);
            
            var acked =true;    
            for(var i=0; i < $this.rxSize; i++){
                if ($this.rxFrames.hasOwnProperty(i) ){
                        txGram.setAckedMask(i,true); // check FrameIds!
                        log("set Ack mask for " + (i+1));
                } else {
                        acked = false;
                        log("unset Ack mask for " + (i+1));
                }
            }

            $this.rxAcked = acked;
            txGram.setAckedWinId(rxGram.getWinId());
            txGram.setDataLength(0);
            if ($this.akFrames.length > 0){
                var anAck = $this.akFrames.peek();
         //       log("Comparing:");
                log(anAck._data);
                log(txGram._data);
                if (anAck.getAckedMask() != txGram.getAckedMask())
                   $this.akFrames.push(txGram);
            } else {
                   $this.akFrames.push(txGram);
            }


        }
        
        // Send up all sequentially received windows
        for(var i=1; i< ARQ_WMAX; i++){
            if (i == 1 // send first frame
                && $this.rxFrames.hasOwnProperty(0)
                && $this.rxFrames[0].isSent() == false){
                    sendUp($this.rxFrames[0].payload());
                    $this.rxFrames[0].setSent(true); // Change sent to function
            }
        
            if ($this.rxFrames.hasOwnProperty(i)
                && $this.rxFrames.hasOwnProperty(i -1)) {
                if ( $this.rxFrames[i-1].isSent()
                    && $this.rxFrames[i].isSent() == false){
                    sendUp($this.rxFrames[i].payload());
                    $this.rxFrames[i].setSent(true); // Change sent to function
                }
            } else {
                break;
            }
        }
    });

    
    var looping = true;
    setInterval(function(){
        var idle = ((time() - timeout) >  ARQ_IDLE);
        var len;
        var data =[];
        if (txq.length > DG_PAYLOAD || txq.length > 0 && idle){
            len = parseInt(Math.min(txq.length,DG_PAYLOAD-1)) ;
            $this.emit('network',new Buffer( txq.splice(0,len),'utf8'),txq.length == 0);
        }
    
        if (looping){
            looping = false;
            setTimeout(function(){
                $this.emit('loop');
                looping = true;

                //log(new Date())
            },$this.txState == ARQ_REQACK  ? Math.round((Math.random()+0.5)*$this.tickInterval) : $this.tickInterval);      
        }
    },100);

}       
   
 
    
ARQ.prototype.incr = function (x,y){
    return (x + y) % (ARQ_MAX);
}

ARQ.prototype.decr = function (x,y){
    return (x - y) % (ARQ_MAX);
}

ARQ.prototype.inRange = function(a,x,y){

 if (x ==y)
        return a == x;
 return a >= x && a <= (y+((x <y)? 0 : ARQ_MAX));
}

ARQ.prototype.incrOffset = function (x) {
    return (x + 1) % (ARQ_WMAX);
}


module.exports.ARQ = ARQ;

