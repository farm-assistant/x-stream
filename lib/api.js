module.exports = XStreamAPI;


/**
 * API Methods for encapsulating/reading XStream request/response packets
 * 
 */

function XStreamAPI() {
	if ( !(this instanceof XStreamAPI) )
		return new XStreamAPI();

}


/**
 * Transforms a Buffer chunk of data to a Base64 string chunk.
 * @param {Buffer} chunk
 * @param {string} encoding - unused since chunk is always a Buffer
 * @param cb
 * @private
 */

XStreamAPI.prototype.request = function(dict){
        return {'REQ':"Foobar"};
}

