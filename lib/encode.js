module.exports = XStreamEncode;

var Transform = require('stream').Transform || require('readable-stream').Transform;
require('util').inherits(XStreamEncode, Transform);

/**
 * Transforms a Buffer stream of binary data to a stream of Base64 text. Note that this will
 * also work on a stream of pure strings, as the Writeable base class will automatically decode
 * text string chunks into Buffers.
 * @returns {XStreamEncode}
 * @constructor
 */
function XStreamEncode() {
	if ( !(this instanceof XStreamEncode) )
		return new XStreamEncode();

 this.BEG = 0x12;
 this.END = 0x13;
 this.DLE = 0x7D;
	this._start =true;
	Transform.call(this);

}


/**
 * Transforms a Buffer chunk of data to a Base64 string chunk.
 * @param {Buffer} chunk
 * @param {string} encoding - unused since chunk is always a Buffer
 * @param cb
 * @private
 */

XStreamEncode.prototype.flush = function(){
        this.push(Buffer([this.END]));
        this._start = true;
}

XStreamEncode.prototype._transform = function (chunk, encoding, cb) {

        if (this._start){
           this.push(Buffer([this.BEG]));
           this._start = false;
        }
        for(var i=0; i < chunk.length; i++){
            if (chunk[i] === this.BEG ||
                chunk[i] === this.END ||
                chunk[i] === this.DLE)
                    this.push(Buffer([this.DLE]));
            this.push(Buffer([chunk[i]]));
        }
    cb();
};

XStreamEncode.prototype._flush = function (cb) {

        this.push(Buffer([this.END]));
        this._start=true;
	cb();
};
