"use strict";

var Events = require('events').EventEmitter;
var util = require('util');
var xbee_api = require('xbee-api');
var C = xbee_api.constants;


function xBeeStream(xbeeAPI,serialport){

    Events.call(this);
    this.xbeeAPI = xbeeAPI;
    this.serialport = serialport;
    var $this = this;

    this.serialport.on('open',function(){
       console.log("Serial port opened: " + serialport.path); 
    });
    
    this.write = function (data,dest) {
        try{
            var frame = {
              id: 0, // We don't want Zigbee level acks
              type: C.FRAME_TYPE.TX_REQUEST_64,
              destination64: dest,
              options: 0x01, // optional, 0x00 is default
              data: data // Can either be string or byte array.
            };
//            console.log(util.inspect(frame));
            $this.serialport.write(xbeeAPI.buildFrame(frame),function(err, res) {
                if (err)
                    throw(err);
              });
        } finally{
            ;
        }
    };
}

xBeeStream.prototype = Object.create(Events.prototype);
module.exports.xBeeStream = xBeeStream;

