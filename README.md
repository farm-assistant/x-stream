# Introduction

X-Stream

Networking Protocol and Utilities to communication with AgSense's Farm-Assistant PCBs 

Includes:

Server process that utilises Serial Port, connects to gateway devices, and maps incoming REUs to UNIX sockets

(See config/config.yaml)

Includes client libraries:

Simple stream to handle encapsulation and deencapsulation of xstream packets.

XStream packets allow the combining of packets over a serial stream where individual transmissions are size bound

Usage

  s = Buffer(xx);
  s.decode(s.encode());

When using within a stream that is not ending (continuous packet)

use

  stream.write(buffer);
  stream.flush(); // to enqueu



/

and
 
  stream.on('data',function(){}).on('flush',function(){});  // to aggregate and handle decoded packets

