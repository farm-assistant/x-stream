// AgSense End node simulator
// Usage: node ./gatewaysim
// Creates a UNIX socket and responds to a set of simple queries of the form {q,"<query type>"} i.e. {q,"GPS"}
// See this.path for name of UNIX socket

var net = require('net');
var fs = require("fs");
var mp = require("../node_modules/msgpack");
var xst = require("../node_modules/x-stream");

Array.prototype.rand = function(){
 return this.length ? this[Math.round(Math.random() * this.length)] : null
}

var _IMAGES = [];
for(i=1;i<=6;i++)
	_IMAGES.push("images/" + i.toString() + ".jpg");
	

function simpleUnix() {
    
    var $this = this;
    this.path = "/tmp/socket";	 // change as required.
    this.socket = undefined;
    this.server = net.createServer(function(socket) {
        if ($this.socket){
            $this.socket.emit('close');
        }
        console.log("Setting up");
        socket.on('close',function(){
           console.log("Socket closed");
           $this.socket = undefined;
           xdc.end();
        });
        xen = xst.encode();
        xdc = xst.decode();
        $this.socket = socket;
        
        $this.socket.pipe(xdc);
        xen.pipe($this.socket);
        var chunks = [];
        xdc.on('data',function(data){
          chunks.push(data); 
        }).on('flush',function(){
           var msg = mp.unpack(Buffer.concat(chunks));
           console.log(msg);

           xen.write(mp.pack($this.processMsg(msg)));
           xen.flush();
           chunks = [];
        });

     });
    
    this.deletePathAsync = function (){
        if (fs.existsSync($this.path)) {
            fs.unlinkSync($this.path);
        }
     };
     
    this.deletePathAsync();

    this.server.listen($this.path,function() {
        console.error('Server listening on ' + $this.path);
    });

    var toggle = true;
    this.processMsg = function(msg) { // Unpacked structure
        var resp = {};
        for(var key in msg.REQ) {
            switch(msg.REQ[key]){
                case 'D1':
                    console.log("D1");
                    console.log(msg.REQ)
                    toggle = msg[msg.REQ[key]] | toggle;
		
                    console.log("Setting D1 to " + toggle);
                    resp[msg.REQ[key]] = toggle;
                    toggle = !toggle;
                    break;
                case 'IMG':
		    var img = _IMAGES.rand();
		    if (typeof img !== 'string') 
			break;
                    resp[msg.REQ[key]] = fs.readFileSync(img);
                    break;
                case 'A1':
                case 'A2':
                    //resp[msg.REQ[key]] = parseInt(Math.random() * 5); // Assumes 5M Max
		    var h = 5 - Math.sin((((new Date().getHours()*100)+(new Date()).getMinutes()*100/60))*Math.PI/2400)*5;
                    resp[msg.REQ[key]] = h
                    break;
                case 'GPS':
                    resp[msg.REQ[key]] = {lon:-46.0848500,lat:167.7720000};  
                    break;
                default:
                    break;
            }
        }
        return resp;
    }
}

simpleUnix();
