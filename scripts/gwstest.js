// AgSense End node simulator
// Usage: node ./gatewaysim
// Creates a UNIX socket and responds to a set of simple queries of the form {q,"<query type>"} i.e. {q,"GPS"}
// See this.path for name of UNIX socket

var net = require('net');
var fs = require("fs");
var mp = require("msgpack");
var xst = require("x-stream");

Array.prototype.rand = function(){
 return this.length ? this[Math.round(Math.random() * this.length)] : null
}

var _IMAGES = [];
for(i=1;i<=6;i++)
	_IMAGES.push("images/" + i.toString() + ".jpg");
	

function simpleUnix() {
    
    var $this = this;
    this.path = "/tmp/sockets/0x123";	 // change as required.
    this.socket = undefined;
    this.server = net.createServer(function(socket) {
        if ($this.socket){
            $this.socket.emit('close');
        }
        console.log("Setting up");
        socket.on('close',function(){
           console.log("Socket closed");
           $this.socket = undefined;
           xdc.end();
        });
        xen = xst.encode();
        xdc = xst.decode();
        $this.socket = socket;
        
        $this.socket.pipe(xdc);
        xen.pipe($this.socket);
        var chunks = [];
        xdc.on('data',function(data){
          chunks.push(data); 
        }).on('flush',function(){
           var msg = mp.unpack(Buffer.concat(chunks));
           console.log(msg);

           xen.write(mp.pack($this.processMsg(msg)));
           xen.flush();
           chunks = [];
        });

     });
    
    this.deletePathAsync = function (){
        if (fs.existsSync($this.path)) {
            fs.unlinkSync($this.path);
        }
     };
     
    this.deletePathAsync();

    this.server.listen($this.path,function() {
        console.error('Server listening on ' + $this.path);
    });

    var toggle = true;
    this.processMsg = function(msg) { // Unpacked structure
        console.log(msg);
        
    }
}

simpleUnix();
