/*global describe, it*/
var decode = require('../lib/decode.js');
var fs = require("fs");
 
fs.createReadStream('fifo', {flags : 'r+'}).pipe(decode()).pipe(process.stdout);

